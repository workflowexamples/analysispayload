# analysis base version from which we start
FROM atlas/analysisbase:21.2.125

# put the code into the repo with the proper owner
COPY --chown=atlas . /code/src

# note that the build directory already exists in /code/src
RUN sudo chown -R atlas /code/src && \
    cd /code/src && \
    source /home/atlas/release_setup.sh && \
    mkdir build && \
    cd build && \
    cmake ../ && \
    make
    
# allows for grid access to VOMS = ATLAS
RUN sudo wget http://repository.egi.eu/sw/production/cas/1/current/repo-files/EGI-trustanchors.repo && \
    sudo mv EGI-trustanchors.repo /etc/yum.repos.d/  && \
    sudo wget http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3 && \
    sudo mv GPG-KEY-EUGridPMA-RPM-3 /etc/pki/rpm-gpg/ && \
    sudo wget http://linuxsoft.cern.ch/wlcg/wlcg-centos7.repo && \
    sudo mv wlcg-centos7.repo /etc/yum.repos.d/ && \
    sudo wget http://linuxsoft.cern.ch/wlcg/RPM-GPG-KEY-wlcg && \
    sudo mv RPM-GPG-KEY-wlcg /etc/pki/rpm-gpg/ 

RUN sudo yum install -y ca-policy-egi-core wlcg-repo.noarch wlcg-voms-atlas CERN-CA-certs voms-clients-cpp \
    sudo yum clean -y all
    
# where you are left after booting the image
WORKDIR /code/src
