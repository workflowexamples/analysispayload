// stdlib functionality
#include <iostream>
#include <fstream>

// ROOT functionality
#include <TFile.h>
#include "TH1F.h"
#include "TCanvas.h"
#include "TString.h"

// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

// for parsing input data files
std::vector<TString> vectorise(TString str, TString sep=" "){
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}

int main(int argc, char *argv[]) {

  // initialize the xAOD EDM
  xAOD::Init();

  // make histogram that we will fill
  TH1F *h_pt = new TH1F("h_pt","h_pt",1000,0,10000);

  // parsing of inputs for getting input files
  TString inputType="direct";  // direct, list
  std::vector<TString> infiles;
  int nevents_max=1000;

  // loop over arguments
  for(int i=1; i<argc; ++i){
    TString arg = TString(argv[i]);
    if(arg.BeginsWith("-")){
      arg.ReplaceAll("-","");
      if(arg=="type"||arg=="t"){
        inputType=TString(argv[i+1]);
        ++i;
        continue;
      }
      else if(arg=="input"||arg=="in"){
        if(inputType=="direct"){
          infiles = vectorise(TString(argv[i+1]),",");
          if(!TString(argv[i+1]).Contains(",")) {
            infiles.clear();
            for(int j=i+1; j<argc; ++j) {
              if(arg.BeginsWith("-")) break;
              infiles.push_back(argv[j]);
              ++i;
            }
          }
        }
        else if(inputType=="list"){
          std::string line;
          std::ifstream myfile(TString(argv[i+1]).Data());
          if (myfile.is_open()){
            while ( std::getline(myfile,line) )
            {
              infiles.push_back(TString(line));
            }
            myfile.close();
          }
        }
        else{
          std::cout << ">>>> Don't understand input type : " << arg << " (EXITING) <<<<" << std::endl;
          return 9;
        }
        continue;
      }
      else if(arg=="n"||arg=="nev"){
        if(TString(argv[i+1]).IsDigit()){
          nevents_max = TString(argv[i+1]).Atoi();
        }
        else{
          std::cout << ">>>> Don't understand nevents argument : " << TString(argv[i+1]) << " (EXITING) <<<<" << std::endl;
          return 9;
        }
      }
      else{
        std::cout << ">>>> Don't understand input argument : " << arg << " (EXITING) <<<<" << std::endl;
        return 9;
      }
    }
  }

  if(infiles.size()<1) {
    infiles = vectorise(TString(argv[1]),",");
    if(!TString(argv[1]).Contains(",")) {
      infiles.clear();
      for(int i=1; i<argc; ++i)
        infiles.push_back(argv[i]);
    }
  }

  if (infiles.size()<1) {
    std::cout << ">>>> Couldn't find any input files (EXITING) <<<<" << std::endl;
    return 9;
  }

  std::cout << "Preparing to run on the following inputs : " << std::endl;
  for ( TString infile : infiles ){
    std::cout << infile << std::endl;
  }
  
  // for counting events
  unsigned count = 0;

  for( TString infile : infiles ){

    std::cout<<"Analyzing File : "<<infile<<std::endl;

    // open the input file
    //TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
    TString inputFilePath = infile;
    xAOD::TEvent event;
    std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
    event.readFrom( iFile.get() );
  
    // get the number of events in the file to loop over
    const Long64_t numEntries = event.getEntries();

    // primary event loop
    for ( Long64_t i=0; i<numEntries; ++i ) {

      // Load the event
      event.getEntry( i );

      // Load xAOD::EventInfo and print the event info
      const xAOD::EventInfo * ei = nullptr;
      event.retrieve( ei, "EventInfo" );
      std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;

      // retrieve the jet container from the event store
      const xAOD::JetContainer* jets = nullptr;
      event.retrieve(jets, "AntiKt4EMTopoJets");

      // loop through all of the jets
      for(const xAOD::Jet* jet : *jets) {
        // print the kinematics of each jet in the event
        std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;
        h_pt->Fill(jet->pt());
      }

      // counter for the number of events analyzed thus far
      count += 1;
      
      // break out when we reach the limit we set for ourselves
      if(count>=nevents_max){
        break;
      }
    }
    
    // break out when we reach the limit we set for ourselves
    if(count>=nevents_max){
      break;
    }
  }
  
  // write out the output analyzed data
  TFile *fout = new TFile("output.root","RECREATE");
  h_pt->Write("myHist");
  fout->Close();
  
  // same a diagnostic plot
  TCanvas *c = new TCanvas("c","c",500,500);
  h_pt->Draw("hist");
  c->SaveAs("plot.pdf");

  // exit from the main function cleanly
  return 0;
}