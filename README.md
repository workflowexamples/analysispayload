# Description
This is an analysis payload that functions using xAOD input files,
as used in ATLAS, but should not be looked at as an analysis framework.
It is only meant to be used in a portion of the template analysis. 
Ultimately, one needs to plug in their own analysis framework in place
of this.

# Building
This is intended to be built upon the `analysisbase:21.2.125` Docker
image.  Start by setting it up :
```
docker run --rm -it -v $PWD:$PWD atlas/analysisbase:21.2.125
```
and navigating to the directory where you cloned this repository. Once 
there, create a build directory and build the code
```
source /home/atlas/release_setup.sh
mkdir build
cd build
cmake ../
make
```
this produces the executable `AnalysisPayload.exe` whose usage can be found below.

# Usage
The `AnalysisPayload.exe` executable has two input arguments
  - `-type` : This toggles how the `-input` argument is used.  If you set this
              to be `direct` then `-input` will expect a comma delimited list
              of paths to xAOD files. If you set this to `list` then it will expect
              a path to a text file which is a newline-delimited list of xAOD inputs.
  - `-input` : This is either a comma-delimited list of paths or a path to a nowline-delimited list
               of input xAOD files, depending on the setting of `-type`.
  - `-nev`   : This is the number of events to be processed at max.
               
               